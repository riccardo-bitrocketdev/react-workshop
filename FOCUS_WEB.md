# Getting Started

Di seguito la documentazione relativa al corso di TypeScript/React/Redux tenuto da [Riccardo Genova](https://www.linkedin.com/in/riccardogenova/).

Si suggerisce l'utilizzo dell'editor [VSCode](https://code.visualstudio.com/).

# Contenuti del corso

## Premesse

- Caratteristiche di un browser
  - Cosa è il _DOM_
  - Il processo di rendering
  - Linguaggi interpretati
- Cosa è _ECMAScript 8_
- _Javascript (.js .jsx)_ VS _Typescript (.ts .tsx)_
- Cosa è _Babel_
- Cosa è _Node/Npm_
- _Webpack_ e il module building

## Indice

- Introduzione a _ReactJS_
  - _DOM_ VS _VirtualDOM_
  - Introduzione al _JSX_
  - _Javascript_ VS _Typescript_
  - Babel e il transpiling
- _Functional Component_ VS _Class Component_
  - Ciclo di vita di un componente
  - Gestione delle props
  - Gestione dell'internal state
- Introduzione agli _hooks_  , _high order components_ e _render props_
- Gli _hooks_ in React
  - _useState_
  - _useEffect_
  - _useCallback_
  - _useMemo_
  - _useContext_
  - _useRef_
- Come creare un _hook_ personalizzato
- State management con _Redux_
  - Reducers
  - Actions
  - Selector
  - Middlewares
- Come utilizzare la _Redux DevTool_
  - Dispatcher
  - States jumping
- Reducer VS State Machines
- Gli _hook_ in _Redux_ 
- _useSelector_
- _useDispatch_
- API asincrone con _Redux-saga_
- HTTP request con _Axios_ e _Fetch_
- Navigazione con _React-router_
- Gli hook in _React-router_
- _useRouteMatch_
- _useHistory_
- _useLocation_
- Multilinga con _React-intl_ VS custom hooks
- Gestione del CSS e delle animazioni con _Styled-components_
- Formattazione con _Prettier_ e _Eslint/TSLint_
- Avvio di un progetto con _create-react-app_
  - Analisi del _package.json_
  - Gestione del work flow
- Documentazione VS _JSDoc_
- _Client_ VS _Server_ side rendering
- Best practices e performance
  - Gestione delle variabili d'ambiente con _.env_
  - Cosa è _Audit_
  - Analisi del bundle con _Webpack-bundle-analyzer_
  - Lazy loading: _lazy_, _Suspense_ e Observer
  - Vantaggi del content hash in _Webpack_
- Design pattern con _Storybook_
- Testing con _Jest_
- End to End testing con _Cypress_
- _Git flow_ e Conventional commit
- Concetti chiave di _React-Native_
- Concetti chiave sistemi Mono repo: Lerna, NX e NPM packages

# Link

## Must have

- [Node](https://nodejs.org/en/)
- [React Chrome DevTool](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
- [Redux Chrome DevTool](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)

## Librerie menzionate

- [React](https://reactjs.org)
- [Redux](https://redux.js.org)
- [Axios](https://github.com/axios/axios)
- [Redux-sagas](https://redux-saga.js.org)
- [React-intl](https://github.com/yahoo/react-intl)
- [React-router](https://reacttraining.com/react-router/web/guides/quick-start)
- [Styled-components](https://www.styled-components.com)
- [Storybook](https://storybook.js.org)
- [Eslint](https://eslint.org)
- [Prettier](https://prettier.io)
- [Cypress](https://www.cypress.io)

## Altre Librerie utili

- [Webpack-bundle-analyzer](https://github.com/webpack-contrib/webpack-bundle-analyzer)
- [React-Placeholder](https://github.com/danilowoz/react-content-loader)
- [Husky](https://github.com/typicode/husky)
- [Npm-check](https://github.com/dylang/npm-check)
- [Google-Map](https://github.com/google-map-react/google-map-react)
- [Google-Map-Autocomplete](https://github.com/hibiken/react-places-autocomplete)
- [React-spring-animation](https://github.com/react-spring/react-spring)

## Strumenti

- [Create-React-App](https://facebook.github.io/create-react-app/docs/getting-started)
- [Bundlephobia](https://bundlephobia.com)

## Altro

- [Webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/repl)
- [React-Native](https://facebook.github.io/react-native/)
- [React-Navigation](https://reactnavigation.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Git](https://git-scm.com/)

# Specifiche

Lo sviluppo è _functional oriented_ . Tutti i componenti saranno sviluppati utilizzando gli hooks, in particolar modo _useState_, _useEffect_, _useCallback_, _useMemo_ .
Stesso discorso vale per l'implementazione di _Redux_ in cui si farà una prima introduzione alla funzione `connect` per poi focalizzarsi sullo sviluppo utilizzando gli hooks _useDispatch_ e _useSelector_ .

## Scrittura del codice

Con estrema attenzione verrà affrontato il processo di architettura di una _tree folder_ e della definizione e controllo delle regole di scrittura del codice utilizzando _Prettier_, _Eslint_ (o _TSLint_ nel caso in cui il corso preveda l'utilizzo di _Typescript_) e _Husky_.
