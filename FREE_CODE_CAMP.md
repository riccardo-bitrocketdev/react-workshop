# Contenuti del corso

Il _freeCodeCamp_ ha una durata di **200 ore**.

## Introduzione (20 ore)

- Caratteristiche di un browser
- Cosa è il DOM
- Il processo di rendering
- Caretteristiche di un Editor
- Configurazione di VSCode

## HTML, CSS e Javascript (80 ore)

- Concetti chiave di HTML e CSS
- Introduzione a ECMAScript 2017
- Gestione di variabili, array, oggetti e funzioni
- Manipolazione del DOM con Javascript
- Cosa sono Node e NPM

## Concetti base di React JS (60 ore)

- Introduzione a _ReactJS_
  - _DOM_ VS _VirtualDOM_
  - Concetti base di _Babel_ e _Webpack_
  - Introduzione al _JSX_
- Introduzione ai _Functional Component_
  - Ciclo di vita di un componente
  - Gestione delle props
  - Gestione dell'internal state
- Gli _hooks_ in React
  - _useState_
  - _useEffect_
  - _useContext_
- Gestione del CSS
- Gestione delle chiamate con _Fetch_
- Navigazione con _React-router_
- Lo State Management con _React Context_

## Git flow (20 ore)

- Problemi del lavoro in team
- Cosa è Git
- Git flow e conventional commit
